package org.example;

import java.util.*;

public class StaticFactoryMethod {
    private final List<Object> list;

    public List<Object> list() {
        return list;
    }

    private StaticFactoryMethod(List<Object> t) {
        this.list = t;
    }

    @SafeVarargs
    public static StaticFactoryMethod of(Object... a) {
        return new StaticFactoryMethod(Arrays.stream(a).toList());
    }

    public static StaticFactoryMethod from(StaticFactoryMethod created) {
        return new StaticFactoryMethod(created.list);
    }

    public static StaticFactoryMethod create(List<Object> list) {
        return new StaticFactoryMethod(list);
    }

    public static  StaticFactoryMethod create() {
        return new StaticFactoryMethod(Collections.emptyList());
    }

}

