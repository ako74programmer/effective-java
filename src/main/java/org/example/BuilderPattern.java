package org.example;

import java.time.LocalDate;

public class BuilderPattern {
    private final String foo;
    private final int bar;
    private final LocalDate date;

    public String foo() {
        return foo;
    }

    public int bar() {
        return bar;
    }

    public LocalDate date() {
        return date;
    }

    public static class Builder {
        // Required parameters
        private final int bar;
        // Optional parameters - initialized to default values
        private String foo = "foo";
        private LocalDate localDate = LocalDate.now();
        public Builder(int bar) {
            this.bar = bar;
        }

        public Builder setLocalDate(LocalDate localDate) {
            this.localDate = localDate;
            return this;
        }

        public Builder setFoo(String foo) {
            this.foo = foo;
            return this;
        }

        public BuilderPattern build() {
            return new BuilderPattern(this);
        }
    }

    private BuilderPattern(Builder builder) {
        this.foo = builder.foo;
        this.bar = builder.bar;
        this.date = builder.localDate;
    }

}
