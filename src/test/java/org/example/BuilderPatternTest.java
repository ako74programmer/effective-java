package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class BuilderPatternTest {

    @Test
    void itShouldCreateBuilderObject() {
        // Given
        LocalDate localDate = LocalDate.of(2020, 1, 2);
        String foo = "foo";
        // When
        BuilderPattern builder = new BuilderPattern.Builder(100)
                .setFoo(foo)
                .setLocalDate(localDate)
                .build();
        // Then
        Assertions.assertAll(
                () -> assertEquals(foo, builder.foo()),
                () -> assertEquals(localDate, builder.date()),
                () -> assertEquals(100, builder.bar())
        );

    }
}