package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


class StaticFactoryMethodTest {

    @Test
    void itShouldCreateObject() {
        // Given & When
        StaticFactoryMethod created = StaticFactoryMethod.create(List.of("one"));
        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(created),
                () -> Assertions.assertEquals("one", created.list().get(0))
        );
    }

    @Test
    void itShouldCreateObjectFromInstance() {
        // Given
        StaticFactoryMethod created = StaticFactoryMethod.create(List.of("one"));
        // When
        StaticFactoryMethod createdFrom = StaticFactoryMethod.from(created);
        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(created),
                () -> Assertions.assertNotNull(createdFrom),
                () -> Assertions.assertEquals("one", createdFrom.list().get(0))
        );
    }

    @Test
    void itShouldCreateObjectOfParameters() {
        // Given & When
        StaticFactoryMethod createOf = StaticFactoryMethod.of(1, 2, 3);
        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(createOf),
                () -> Assertions.assertEquals(List.of(1, 2, 3), createOf.list())
        );
    }

    @Test
    void itShouldCreateObjectWithNullList() {
        // Given & When
        StaticFactoryMethod createWithNullList = StaticFactoryMethod.create(null);
        // Then
        Assertions.assertNull(createWithNullList.list());
    }

    @Test
    void itShouldCreateObjectWithEmptyList() {
        // Given & When
        StaticFactoryMethod createWithEmptyList = StaticFactoryMethod.create();
        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(createWithEmptyList),
                () -> Assertions.assertEquals(0, createWithEmptyList.list().size())
        );

    }
}